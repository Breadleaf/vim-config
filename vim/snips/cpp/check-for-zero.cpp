bool checkForZero(double number)
{
	int zeroPresent = false;
	
	if ( number == 0 )
	{
		zeroPresent = true;
	}

	return zeroPresent;
}
