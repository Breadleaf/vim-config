void swapPosition(int array[], int firstPosition, int secondPosition)
{
	int temp = array[firstPosition];
	array[firstPosition] = array[secondPosition];
	array[secondPosition] = temp;
}
