#include <iostream>
using namespace std;

#include <SFML/Graphics.hpp>
using namespace sf;


int main()
{
	const int WIDTH(600), HEIGHT(800);
	RenderWindow window(VideoMode(WIDTH, HEIGHT), "Window Name");

	// Process files below

	// Process files above

	while (window.isOpen())
	{
		window.clear(Color::Black);

		// Draw things to the screen below

		// Draw things to the screen above

		window.display();

		Event event;
		while (window.pollEvent(event))
		{
			if (event.type == Event::Closed)
			{
				window.close();
			}
		}
	}

	return EXIT_SUCCESS;

}
