void leastToMostBubbleSort(int array[], int length)
{
	cout << "Made it here! (L2M)" << endl;

	for ( int i = 0; i <= length-1; i++ )
	{
		for ( int j = 0; j <= length-1; j++ )
		{
			if ( i != j )
			{
				cout << "Current pair being sorted: " 
					<< i << " " << j << endl;

				if ( array[j] > array[i] )
				{
					swapPosition(array, i, j);
				}
				printArrayInOrder(array, length, true);
			}
		}
	}
}
