void printArrayInOrder(int array[], int length, bool extraNewLine)
{
	for ( int i = 0; i <= length-1; i++ )
	{
		cout << array[i] << " ";
	}
	
	if ( extraNewLine == true )
	{
		cout << endl;
	}

	cout << endl;
}
