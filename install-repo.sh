#!/usr/bin/bash

echo "Note: you will still need to follow setup commands found in ~/.vimrc"
cp -ri vim/* ~/.vim
cp -i vimrc ~/.vimrc
