#!/usr/bin/bash

# Clean up repo
if test -d ./vim/; then
	rm -rf ./vim/
fi

if test -f ./vimrc; then
	rm -f ./vimrc
fi

# Copy files to repo
cp ~/.vimrc ./vimrc

mkdir vim
mkdir vim/UltiSnips
mkdir vim/ftdetect
mkdir vim/syntax
mkdir vim/snips
mkdir vim/ycm_extra_config

cp -r ~/.vim/UltiSnips/* vim/UltiSnips
cp -r ~/.vim/ftdetect/* vim/ftdetect
cp -r ~/.vim/syntax/* vim/syntax
cp -r ~/.vim/snips/* vim/snips
cp ~/.vim/ycm_extra_config/.ycm_extra_conf.py vim/ycm_extra_config/.ycm_extra_conf.py

# Update repo
git add --all
git commit -m "Updated repo"
git push
